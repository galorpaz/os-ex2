CC=g++
FLAGS=-Wall

libpft.a: pft.cpp
	$(CC) $(FLAGS) -c pft.cpp
	ar rvs libpft.a pft.o

all: libpft.a

tar:
	tar -cvf ex2.tar compParaLevel.jpg README Makefile pft.cpp

clean:
	rm -f *.o *.a *~ ex2.tar

.PHONY: all tar clean
