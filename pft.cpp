#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <unistd.h>
#include "pft.h"
#include <sys/time.h>
#include <algorithm>
#include <cmath>
#include <sstream>

using namespace std;

#define SIZE_OF_CHUNK  30
#define READ 0
#define WRITE 1
#define FIRST_CHILD_INDEX 0
#define BUFFER_SIZE 1

int file_num;
int level;
string error_data;
double process_time;

/*
Initialize the pft library.
Argument:
        "n" is the level of parallelism to use (number of parallel ~@~Xfile~@~Y) commands.
This method should initialize the library with empty statistics.

A failure may happen if a system call fails (e.g. alloc) or if n is not positive.
Return value:
        A valid error message, started with "pft_init error:" should be obtained by using the pft_get_error().
 */
int pft_init(int n)
{
	process_time=0;
	file_num = 0;
	error_data ="";
	level=n;

	if (n < 0)
	{
		error_data = "pft_init error: ParallelismLevel smaller then 0";
		pft_get_error();
	}
	return 0;
}

/*
This function called when the user finished to use the library.
Its purpose is to free the memory/resources that allocated by the library.

The function return error FAILURE if a system call fails.
Return value:


The function return error FAILURE if a system call fails.
Return value:
        On success return SUCCESS, on error return FAILURE.
        A valid error message, started with "pft_done error:" should be obtained by using the pft_get_error().
 */
int pft_done()
{
	return SUCCESS;
}


/*
Set the parallelism level.
Argument:
        "n" is the level of parallelism to use (number of parallel ~@~Xfile~@~Y) commands.
A failure may happen if a system call fails (e.g. alloc) or n is not positive.
Return value:
        On success return SUCCESS, on error return FAILURE.
        A valid error message, started with "setParallelismLevel error:" should be obtained by using the
         pft_get_error().
 */
int setParallelismLevel(int n)
{
	return pft_init(n);
}

/*
 * Return the last error message.
 * The message should be empty if there was no error since the last initialization.
 * This function must not fail.
 */
const std::string pft_get_error()
{
	std::string mystring(error_data);
	return mystring;
}

/*
This method initialize the given pft_stats_struct with the current statistics collected by the pft library.

If statistic is not null, the function assumes that the given structure have been allocated,
and updates file_num to contain the total number of files processed up to now
and time_sec to contain the total time in seconds spent in processing.

A failure may happen if a system call fails or if the statistic is null.

For example, if I call "pft_init", wait 3 seconds, call "pft_find_types" that read 7,000,000 files in 5 seconds,
 and then call to this
method, the statistics int good; should be: statistic->time_sec = 5 and statistic->file_num = 7,000,000.

Parameter:
        statistic - a pft_stats_struct structure that will be initialized with the current statistics.
Return value:
        On success return SUCCESS, on error return FAILURE.
        A valid error message, started with "pft_get_stats error:" should be obtained by using the 
        pft_get_error().
 */
int pft_get_stats(pft_stats_struct *statistic)
{
	if (statistic != NULL)
	{
		statistic->file_num = file_num;
		statistic->time_sec = process_time;
		return SUCCESS;
	}
	else
	{
		error_data = "pft_get_stats error: statistic is null";
		pft_get_error();
		return FAILURE;
	}
}


/*
 * Clear the statistics setting all to 0.
 * The function must not fail.
 */
void pft_clear_stats()
{
	file_num=0;
	process_time=0;
}

/**
 * this method read the child output from the pipe type_pipe and write it to the typed_vec
 *
 * Parmas:
 * fd : the pipe to read from
 * chunk_size : the amount of files that needs to be read
 * types_vec : reference to the vector we need to write the data to
 * index_to_write : the index in  types_vec we need to write to
 *
 * Return:
 * -1 on failure
 * 0 on success
 */
int readFromChild(int fd, int chunk_size, std::vector<std::string>& types_vec, int index_to_write){
	char buf;
	stringstream string_streamer;
	string type_to_write = "";
	int file_index;
	for (file_index=0; file_index < chunk_size; file_index++)
	{
		string_streamer.str("");
		int retval = read(fd,&buf,BUFFER_SIZE);
		while (buf != '\n') {
			if (retval == FAILURE){
				return FAILURE;
			}
			if (retval == 0) {
				break;
			}
			string_streamer<<buf;
			retval = read(fd,&buf,BUFFER_SIZE);
		}
		type_to_write = string_streamer.str();
		types_vec[index_to_write+file_index] = type_to_write;
	}
	return SUCCESS;
}

/*
 * this method set the number of files that each child will deal with
 *
 * Params:
 * first_chunk : the size of the first chunk to be used by the first child
 *
 * Return:
 * the regular chunk size to be used by all other child
 */
int setChunkSize(int& first_chunk, int& amount_of_chunk) {
	int reminder;
	if (SIZE_OF_CHUNK*level <= file_num){
		//in this case we need the first child will get bigger chunk
		reminder = file_num%SIZE_OF_CHUNK;
		first_chunk = SIZE_OF_CHUNK+reminder;
		amount_of_chunk = file_num/SIZE_OF_CHUNK;
		return SIZE_OF_CHUNK;
	}
	else{
		//in this case each child get one chunk
		reminder = file_num%level;
		first_chunk = file_num/level +reminder;
		amount_of_chunk = level;
		return file_num/level;
	}
}
/**
 * this method supplies a chunk of files, basically it write the amount of data required for each child process
 *
 * Params:
 * chunk_size : the number of files that will be written in the pipe
 * file_names_vec : the vector of the files name that needed to be written
 * fd_to_write : the number of the file descriptor refering to the write side of the pipe we want to write to
 * position :  the position in the file_names_vec, we need to start writing from this position
 *
 * Return:
 * -1 on failure
 * 0 on success
 */
int fileSupplayer(int chunk_size, std::vector<std::string>& file_names_vec, int fd_to_write, int position) {

	int file_index;
	stringstream string_stremer;
	string file_to_write="";
	for (file_index = 0; file_index < chunk_size; file_index++){
		string_stremer.str("");
		string_stremer << file_names_vec.at(position + file_index) << "\n";
		file_to_write = string_stremer.str();
		int retval = write(fd_to_write, file_to_write.c_str(), file_to_write.length());
		if (retval == FAILURE) {
			return FAILURE;
		}
	}
	return position+chunk_size;
}

/*
This function uses ~@~Xfile~@~Y to calculate the type of each file in the given vector using n parallelism level.
It gets a vector contains the name of the files to check (file_names_vec) and an empty vector (types_vec).
The function runs "file" command on each file in the file_names_vec (even if it is not a valid file) using 
n parallelism level,
and insert its result to the same index in types_vec.

The function fails if any of his parameters is null, if types_vec is not an empty vector or if a system 
called failed
(for example fork failed).

Parameters:
        file_names_vec - a vector contains the absolute or relative paths of the files to check.
        types_vec - an empty vector that will be initialized with the results of "file" command on each file 
        in file_names_vec.
Return value:
        On success return SUCCESS, on error return FAILURE.
        A valid error message, started with "pft_find_types error:" should be obtained by using the 
        pft_get_error().
 */
int pft_find_types(std::vector<std::string>& file_names_vec, std::vector<std::string>& types_vec)
{
	int first_chunk = 0;
	int new_chunk_size =0;
	pid_t cpid;
	struct timeval time_out;//using in select system call
	time_out.tv_sec = 1;
	time_out.tv_usec = 0;
	struct timeval time_before;//using for statistic
	struct timeval time_after;//using for statistic
	struct timeval time_diff;
	int file_pipe[level][2];//parent write the file and child read the file
	int type_pipe[level][2];//child write the file type and parent reads
	int current_position = 0;//position of the file_vec
	types_vec.assign(file_names_vec.size(),""); //Initializing type_vec to be empty
	int child_to_position[level]; //save the position in the file_name_vector each child started to read from
	int amount_of_chunk = 0;

	//the following check that file_names_vec and types_vec were initialized
	if (file_names_vec.size() == 0 || types_vec.size() == 0)
	{
		error_data = "pft_find_types error: input vectors are null";
		pft_get_error();
		return FAILURE;
	}

	file_num = file_names_vec.size();

	//the following checks that types_vec is not empty
	vector<string>::const_iterator file_iter;
	for (file_iter = types_vec.begin() ; file_iter != types_vec.end(); file_iter++)
	{
		if ((*file_iter).compare("") != 0) {
			error_data = "pft_find_types error: input types_vec is not empty";
			pft_get_error();
			return FAILURE;
		}
	}
	//set chunk sizes
	new_chunk_size = setChunkSize(first_chunk, amount_of_chunk);
	int chunk_to_write = amount_of_chunk;

	int current_child_index;
	for (current_child_index = 0; current_child_index < level; current_child_index++){
		//creating two pipes
		int ret_val_file_pipe = pipe(file_pipe[current_child_index]);
		int ret_val_type_pipe = pipe(type_pipe[current_child_index]);
		//check if pipe failed
		if(ret_val_file_pipe == -1 || ret_val_type_pipe==-1) {
			error_data = "pft_find_types error: pipe failed";
			pft_get_error();
			return FAILURE;
		}
	}

	for (current_child_index = 0; current_child_index < level; current_child_index++){
		//fork
		cpid = fork();
		if (cpid == FAILURE) {
			error_data = "pft_find_types error: fork failure";
			int file_Index;
			//closing all pipes
			for (file_Index = 0; file_Index<level; file_Index++) {
				close(file_pipe[file_Index][READ]);
				close(file_pipe[file_Index][WRITE]);
				close(type_pipe[file_Index][READ]);
				close(type_pipe[file_Index][WRITE]);
			}
			pft_get_error();
			return FAILURE;
		}

		//child goes here
		if (cpid == 0)
		{
			//closing all the other pipes
			int other_child_index;
			for (other_child_index = 0; other_child_index<level; other_child_index++){
				//closing the unused pipes
				if(other_child_index != current_child_index){
					close (type_pipe[other_child_index][READ]);
					close (type_pipe[other_child_index][WRITE]);
					close (file_pipe[other_child_index][READ]);
					close (file_pipe[other_child_index][WRITE]);
				}
			}

			close (type_pipe[current_child_index][READ]); //close the reading side of the type_pipe
			close (file_pipe[current_child_index][WRITE]); // close the writing side of the file_pipe
			int retval1 = dup2(type_pipe[current_child_index][WRITE], STDOUT_FILENO); 
			// replacing std_out with the writing side of the pipe
			int retval2 = dup2(file_pipe[current_child_index][READ], STDIN_FILENO);
			if (retval1==FAILURE){

				error_data = "pft_find_types error: dup2 failed";
				pft_get_error();
			}
			if (retval2 == FAILURE){

				error_data = "pft_find_types error: dup2 failed";
				pft_get_error();
			}
			execl("/usr/bin/file", "file", "-n", "-f", "-", NULL);

			//if execl failed
			error_data = "pft_find_types error: execl failed";
			close(type_pipe[current_child_index][WRITE]);
			close(file_pipe[current_child_index][READ]);
			pft_get_error();
		}
		else {
			//parent goes here
			close(type_pipe[current_child_index][WRITE]);
			close(file_pipe[current_child_index][READ]);
		}
	}

	//get the starting time
	if(gettimeofday(&time_before, NULL) == FAILURE)
	{
		error_data = "pft_find_types error: gettimeofday failed";
		pft_get_error();
		return FAILURE;
	}

	//only parent process from this point and on
	for (current_child_index = 0; current_child_index < level; current_child_index++){
		if (current_child_index == FIRST_CHILD_INDEX) {
			//first child goes here
			child_to_position[current_child_index] = current_position;
			if (chunk_to_write>0){
				current_position = fileSupplayer(first_chunk, file_names_vec, 
				file_pipe[current_child_index][WRITE], FIRST_CHILD_INDEX);
				chunk_to_write--;

				if (current_position == FAILURE){
					error_data = "pft_find_types error: write to first pipe failed";
					pft_get_error();
				}
			}
		}
		else {
			if (chunk_to_write > 0){
				child_to_position[current_child_index] = current_position;
				current_position = fileSupplayer(new_chunk_size, file_names_vec, 
				file_pipe[current_child_index][WRITE],current_position);
				chunk_to_write --;
				if (current_position == FAILURE){
					error_data = "pft_find_types error: write to other pipe failed";
					pft_get_error();
				}

			}
		}
	}

	//we want to check which child finish to read the files
	fd_set fd, readyfd;
	int child_index;
	//creating fd_set from the type_pipe
	FD_ZERO(&fd);
	for(child_index = 0; child_index<level; child_index++){
		FD_SET(type_pipe[child_index][READ],&fd);
	}
	int first_read = 0;
	while (true){
		//backup the fd's
		memcpy(&readyfd, &fd, sizeof(fd));
		int num_of_ready_files = select(type_pipe[level-1][READ]+1, &readyfd, NULL, NULL, &time_out);
		int index_to_write = 0;
		if (num_of_ready_files > 0){
			for(child_index = 0; child_index < level; child_index++){
				//iterating over the fd's that are ready
				if(FD_ISSET(type_pipe[child_index][READ],&readyfd)){
					index_to_write = child_to_position[child_index];
					if (child_index == FIRST_CHILD_INDEX && first_read == 0){
						//first child go here
						readFromChild(type_pipe[child_index][READ],first_chunk, types_vec, index_to_write);
						first_read = 1;
						amount_of_chunk--;
					}
					else {
						//other child go here
						readFromChild(type_pipe[child_index][READ],new_chunk_size, types_vec, index_to_write);
						amount_of_chunk--;
					}
					if (chunk_to_write>0){
						//there are more chunks to take care of
						child_to_position[child_index] = current_position;
						current_position = fileSupplayer(new_chunk_size, file_names_vec, 
						file_pipe[child_index][WRITE],current_position);
						chunk_to_write --;

					}
				}
				//clear the fd that were already has been read
			FD_CLR(type_pipe[child_index][READ],&readyfd);
			}

		}
		//get the ending time
		if(gettimeofday(&time_after, NULL) == FAILURE)
		{
			error_data = "pft_find_types error: gettimeofday failed";
			pft_get_error();
			return FAILURE;
		}

		timersub(&time_after,&time_before,&time_diff);
		process_time = time_diff.tv_usec/1000000.0 + time_diff.tv_sec;

		if (amount_of_chunk==0){//no more chunks to take care of
			int file_Index;
			for (file_Index = 0; file_Index<level; file_Index++) {
				close(file_pipe[file_Index][READ]);
				close(file_pipe[file_Index][WRITE]);
				close(type_pipe[file_Index][READ]);
				close(type_pipe[file_Index][WRITE]);
			}
			return SUCCESS;
		}
	}
	return SUCCESS;
}
